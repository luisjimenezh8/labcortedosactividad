package com.example.labcortedosactividad;

import androidx.appcompat.app.AppCompatActivity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.labcortedosactividad.ui.home.JobJob;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private EditText editTextUser;
    private EditText editTextPassword;
    private String user;
    private String password;
    private Button btnIngresar;
    private ProgressBar progreso;

    private final static int ID_SERVICE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enlazarViews();

        initJobScheduler();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("TAG","1");
//                ComponentName componentName = new ComponentName(getApplicationContext(), JobJob.class);
//                JobInfo info;
//                Log.d("TAG","2");
//                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
//                    Log.d("TAG","3");
//                    info = new JobInfo.Builder(ID_SERVICE,componentName)
//                            .setRequiresCharging(true)
//                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                            .setPersisted(true)
//                            .setMinimumLatency(5*1000)
//                            .build();
//                } else {
//                    Log.d("TAG","4");
//                    info = new JobInfo.Builder(ID_SERVICE,componentName)
//                            .setRequiresCharging(true)
//                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                            .setPersisted(true)
//                            .setPeriodic(5*1000)
//                            .build();
//                }
//                Log.d("TAG","5");
//                JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
//                int result = jobScheduler.schedule(info);
//                Log.d("TAG","6");
//                if(result == JobScheduler.RESULT_SUCCESS) {
//                    Log.d("TAG","Job Acabado");
//                }else{
//                    Log.d("TAG","Job ha fallado");
//                    JobScheduler schedulerCancell = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
//                    schedulerCancell.cancel(ID_SERVICE);
//                    Log.d("TAG","Job Cancelado");
//                }
                new Task1().execute(editTextUser.getText().toString());
                user = editTextUser.getText().toString();
                password = editTextPassword.getText().toString();
            }
        });

    }

    private final static int DONWLOAD_JOB_KEY = 101;

    private void initJobScheduler() {
        Log.d("MainActivity","1");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            ComponentName componentName = new ComponentName(this,JobJob.class);
            PersistableBundle bundle = new PersistableBundle();
            bundle.putInt("number",10);
            JobInfo.Builder builder = new JobInfo.Builder(DONWLOAD_JOB_KEY,componentName)
                    .setExtras(bundle)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true);
//                    .setPeriodic(15*60*1000);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                Log.d("TAG","if");
                builder.setPeriodic(60*1000,30*60*1000);
            }else{
                Log.d("TAG","else");
                builder.setPeriodic(60*1000);
            }

            JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
            scheduler.schedule(builder.build());

        }
    }

    private void enlazarViews(){
        editTextUser = (EditText) findViewById(R.id.editTextUser);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        btnIngresar = (Button) findViewById(R.id.button);
        progreso = (ProgressBar) findViewById(R.id.progressBar);

    }

    private void login(){

        if ((user.equals("admin"))&&(password.equals("admin"))){

            Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            String datetime = dateformat.format(c.getTime());

            startActivity(intent);

            Toast.makeText(this,"Ingreso con exito",Toast.LENGTH_LONG).show();

        }else {
            Toast.makeText(this,"datos erroneos o faltantes",Toast.LENGTH_LONG).show();
        }

    }

    private class Task1 extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            progreso.setVisibility(View.VISIBLE);
            btnIngresar.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String s) {

            progreso.setVisibility(View.INVISIBLE);
            btnIngresar.setEnabled(true);
            login();

        }

        @Override
        protected String doInBackground(String... strings) {

            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            return strings[0];
        }
    }

}