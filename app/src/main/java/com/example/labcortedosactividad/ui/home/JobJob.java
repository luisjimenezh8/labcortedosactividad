package com.example.labcortedosactividad.ui.home;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.labcortedosactividad.Interface.JsonPlaceHolderAPI;
import com.example.labcortedosactividad.SQLlite.RecursosBD;
import com.example.labcortedosactividad.SQLlite.SQLmetodos;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobJob extends JobService {

    private boolean jobCancelled = false;
    private final String URL = "https://www.datos.gov.co/resource/";
    private  List<Posts> objvacunacionList2;
    private TaskOnly taskOnly;
    private JobParameters parameters;
    private SQLmetodos conn;
    @Override
    public boolean onStartJob(JobParameters params) {
        this.parameters = params;
        //Es invocado por el sistema para la hora de hacer el trabajo
        Log.d("TAG","Inicio");
//        doBackWork(params);
//        taskOnly = new TaskOnly();
//        taskOnly.execute(10);
        objvacunacionList2 = new ArrayList<>();
        Log.d("TAG",objvacunacionList2.isEmpty() + "");
        conn = new SQLmetodos(this,"bd_puestos",null,1);
        new TaskPost().execute(objvacunacionList2);
        return true;
    }
    private class TaskPost extends AsyncTask<List<Posts>,Void,List<Posts>>{

        @Override
        protected List<Posts> doInBackground(List<Posts>... lists) {
            Log.d("TAG","Init");
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL)
                    .build();
            Log.d("TAG",retrofit.toString());
            JsonPlaceHolderAPI jsonPlaceHolderAPI = retrofit.create(JsonPlaceHolderAPI.class);
            Call<List<Posts>> call = jsonPlaceHolderAPI.getPost();
            Log.d("TAG", jsonPlaceHolderAPI.getPost().toString());
            call.enqueue(new Callback<List<Posts>>() {
                @Override
                public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {
                    if(!response.isSuccessful()){
//                        Toast.makeText(root.getContext(),"Codigo: " + response.code(),Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.d("TAG",objvacunacionList2.isEmpty() + "");
                    objvacunacionList2 = response.body();
                    String content = "";
                    for (Posts post : objvacunacionList2) {
//                        Log.d("TAG",post.getMuni_nombre());
                        if(post.getMuni_nombre().equalsIgnoreCase("medellín")){
//                            saveSQL(post);
                            if(saveSQL(post)){
                                break;
                            }
                        }
                        Log.d("FOR","post");
                    }

//                    customAdapterPosts = new CustomAdapterPosts(objvacunacionList2,root.getContext(), LugaresVacunacionFragment.this::seleccion);
//                    recyclerViewPosts.setAdapter(customAdapterPosts);

                }

                @Override
                public void onFailure(Call<List<Posts>> call, Throwable t) {
                    System.out.println(t);
                }
            });
            return objvacunacionList2;
        }

        @Override
        protected void onPostExecute(List<Posts> posts) {
            super.onPostExecute(posts);
//            Log.d("TAG","onPostExecute: msj: " + s);
            jobFinished(parameters,true);
        }
    }


    public boolean saveSQL(Posts objvacunacion) {
        SQLiteDatabase db =conn.getWritableDatabase();
        SQLiteDatabase read=conn.getReadableDatabase();
        Cursor cursor=read.rawQuery("SELECT * FROM "+ RecursosBD.getTablaPuestos()+" WHERE "+RecursosBD.getCampSede()+" = "+ "'"+objvacunacion.getSede_nombre()+"'",null);
        if(cursor.getCount()==0){
            ContentValues values = new ContentValues();
            values.put(RecursosBD.getCampDepartamento(),objvacunacion.getDepa_nombre());
            values.put(RecursosBD.getCampMunicipio(),objvacunacion.getMuni_nombre());
            values.put(RecursosBD.getCampSede(),objvacunacion.getSede_nombre());
            values.put(RecursosBD.getCampDireccion(),objvacunacion.getDireccion());
            values.put(RecursosBD.getCampTelefono(),objvacunacion.getTelefono());
            values.put(RecursosBD.getCampEmail(),objvacunacion.getEmail());
            values.put(RecursosBD.getCampNajuNombre(),objvacunacion.getNaju_nombre());
            values.put(RecursosBD.getCampFechaCorte(),objvacunacion.getFecha_corte_reps());
            Long idres=db.insert(RecursosBD.getTablaPuestos(),RecursosBD.getCampId(),values);
            System.out.println("id registro: "+idres);
            Log.d("SQL","Dato saved");
            return true;
//            Toast.makeText(root.getContext(),"Datos guardados satisfactoriamente",Toast.LENGTH_SHORT).show();
        }
        else{
            Log.d("SQL","Error, el objeto ya existes");
            return false;
//            Toast.makeText(root.getContext(),"Error, el objeto fue guardado anteriormente",Toast.LENGTH_SHORT).show();
        }
    }
//    private void doBackWork(/*final*/JobParameters params) {
//        Log.d("TAG","doBackWork");
//        new Thread(new Runnable() {//Abrimos un nuevo hilo
//            @Override
//            public void run() {//Corre el hilo
//                //Acá dentro conrrería lo que querramos hacer
//                //En mi caso la llamada a la API para los casos
//                for(int i = 0; i < 10;i++) {
//                    if(jobCancelled){
//                        return;
//                    }
//                    Log.d("TAG","RUN: " + i);
//                    try {
//                        Thread.sleep(1000);
//                    }catch (InterruptedException e) {
//
//                    }
//                    Log.d("TAG","Job Finished");
//                    jobFinished(params,false);
//                }
//            }
//        }).start();
//    }

    @Override
    public boolean onStopJob(JobParameters params) {
//        Log.d("TAG","onStopJob");
//        jobCancelled = true;

        Log.d("TAG","Canceled");
        if(null != taskOnly){
            if(!taskOnly.isCancelled()){
                taskOnly.cancel(true);
            }
        }
        return false;
    }

    private class TaskOnly extends AsyncTask<Integer, Integer, String> {

        @Override
        protected String doInBackground(Integer... integers) {
            for(int i=0; i<integers[0];i++){
                SystemClock.sleep(1000);
                publishProgress(i);
            }
            return "Job Finished";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            Log.d("TAG", "onProgressUpdate: i was: " + values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TAG","onPostExecute: msj: " + s);
            jobFinished(parameters,true);
        }
    }
}
